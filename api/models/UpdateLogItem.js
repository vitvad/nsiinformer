/**
 * UpdateLogItem.js
 *
 * @description :: This is a log item model
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
	
	connection: 'someMongodbServer',

	attributes: {
		updateDetectedAt: 'datetime',
		bookTitle: 'string',
		authorName: 'string',
		description: 'string',
		writer: {
            model: 'Writer'
        },
		book: {
            model: 'Book'
        },
	}
};