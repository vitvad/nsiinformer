/**
 * Writer.js
 *
 * @description :: This is a writer (author) model
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

	connection: 'someMongodbServer',

	attributes: {
		category: 'string',
		siteTemplate: 'string',
		site: 'string',
		path: 'string',
		name: 'string',
		href: 'string',
		imageUrl: 'string',
		imagePath: 'string',
		checkUpdates: 'boolean',
		checkUpdatesAt: 'datetime',
		hasUpdates: 'boolean',
		updateDetectedAt: 'datetime',
		books: {
			collection: 'Book',
			via: 'writer'
		},
		logs: {
			collection: 'UpdateLogItem',
			via: 'writer'
		}

	}
};