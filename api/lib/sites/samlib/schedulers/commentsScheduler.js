// minimum interval in seconds between original site http queries (if it has limitations and blocks for rapid queries, i.e. Samlib.ru)
exports.httpQueryInterval = 10;
// updates plan according comments activity
function getNewCheckDate(dateDiffInMinutes) {
    var minutes = 0;
    if (!minutes) minutes = (dateDiffInMinutes >= 0 && dateDiffInMinutes <= 2) && 1;
    if (!minutes) minutes = (dateDiffInMinutes >= 3 && dateDiffInMinutes <= 5) && 2;
    if (!minutes) minutes = (dateDiffInMinutes >= 6 && dateDiffInMinutes <= 10) && 3;
    if (!minutes) minutes = (dateDiffInMinutes >= 11 && dateDiffInMinutes <= 15) && 5;
    if (!minutes) minutes = (dateDiffInMinutes >= 16 && dateDiffInMinutes <= 60) && 10;
    if (!minutes) minutes = (dateDiffInMinutes >= 61) && 30;

    if (!minutes) minutes = 1;
    return (new Date()).addMinutes(minutes);
}

exports.calculateNextCheckTime = function(book) {
    var lastUpdate = book.commentsUpdateDetectedAt;
    var now = new Date();
    var dateDiffInMinutes = Math.abs(Utils.dateDiffInMinutes(lastUpdate, now));
    book.checkCommentsAt = getNewCheckDate(dateDiffInMinutes);
    return book;
}