// middleware to save data to any storage


// saves parsed author object
function save(author, site, callback) {
    Writer.findOrCreate({
        path: author.path,
        siteTemplate: author.siteTemplate
    })
        .exec(function(err, writer) {
            if (err) {
                return callback('[authorStorage] Error> ' + err);
            }
            if (!writer) {
                return callback('[authorStorage] Error> ' + 'was unable to create Writer object');
            }
            var oldHasUpdatesStatus = writer.hasUpdates;
            writer.siteTemplate = author.siteTemplate;
            writer.site = author.site;
            writer.path = author.path;
            writer.name = author.name;
            writer.href = author.href;
            writer.imageUrl = author.image;
            if (author.checkUpdates !== undefined && author.checkUpdates !== null)
                writer.checkUpdates = author.checkUpdates;
            if (writer.checkUpdates == undefined && writer.checkUpdates == null)
                writer.checkUpdates = true;

            var save = function(obj, cb) {
                obj.save(function(err) {
                    var result = {};
                    if (err) {
                        var e = '[authorStorage] Error (' + (author.href) + ')> ' + err;
                        console.log(e);
                        result.success = false;
                        result.error = e;
                    } else {
                        console.log('[authorStorage] SUCCESS saving ' + obj.path + '. Has updates: ' + (obj.hasUpdates ? 'YES' : ' NO'));
                        result.success = true;
                    }
                    return cb(null, result);
                });

            };

            var updateBooks = function(cb) {
                if (!author.texts)
                    return cb();
                if (author.texts.length === 0)
                    return cb();

                var text = author.texts.pop();

                Book.findOrCreate({
                    path: (author.path + '/' + text.link),
                    siteTemplate: author.siteTemplate
                })
                    .exec(function(err, book) {
                        if (err) {
                            return cb('[authorStorage] Error> ' + err);
                        }
                        if (!book) {
                            return cb('[authorStorage] Error> ' + 'was unable to create book object');
                        }

                        var oldSize = book.size;
                        var oldTitle = book.title;
                        var oldAnnotationHash = book.annotationHash;
                        var oldAnnotation = book.annotation;
                        var newAnnotation = text.annotation;
                        var newAnnotationHash = Utils.hashAnnotation(text.annotation);
                        var newHash = Utils.getHash(text.name + text.size + newAnnotationHash);
                        if (book.checkUpdates == undefined && book.checkUpdates == null)
                            book.checkUpdates = true;



                        book.title = text.name;
                        book.path = (author.path + '/' + text.link);
                        book.href = (Utils.stripUrl(author.href) + '/' + text.link);
                        book.site = author.site;
                        book.siteTemplate = author.siteTemplate;                        
                        book.size = text.size;
                        book.annotation = text.annotation;
                        book.annotationHtml = text.annotationHtml;
                        book.annotationHash = newAnnotationHash;
                        book.rating = text.rating;
                        book.group = text.group;
                        book.categories = text.categories;
                        book.hash = newHash;

                        var bookHasUpdates = (oldSize && oldTitle) && ((oldSize !== text.size) || (oldTitle !== text.name)); // (oldSize && oldTitle)- skip just added book           
                        bookHasUpdates = bookHasUpdates || (oldAnnotationHash !== newAnnotationHash);
                        if (!bookHasUpdates) // convert undefined and null to false
                            bookHasUpdates = false;

                        // if we detect new updates, remember date-time
                        if (bookHasUpdates) {
                            book.updateDetectedAt = new Date();
                            writer.updateDetectedAt = book.updateDetectedAt;
                            book.prevSize = oldSize;
                        }
                        // just marker that book has unread updates
                        book.hasUpdates = book.hasUpdates || bookHasUpdates;
                        // writer has updates (marker)
                        writer.hasUpdates = writer.hasUpdates || book.hasUpdates;
                        if (text.comments) {
                            if (text.comments.description) {
                                book.commentsDescription = text.comments.description;
                            }
                            if (text.comments.path) {
                                book.commentsPath = text.comments.path;
                            }
                        }

                        writer.books.add(book);


                        // save book and go to the next steps of chain
                        var saveBook = function() {
                            save(book, function(er, res) {
                                if (er) {
                                    return callback(er);
                                }
                                Book.publishUpdate(book.id, book);
                                updateBooks(cb);
                            });
                        };
                        // update logs
                        //=======================================
                        if (bookHasUpdates) {
                            // what changes?
                            var changes = '';
                            if (oldSize !== text.size) {
                                changes = 'Size has changed from ' + oldSize + ' to ' + text.size + '; ';
                            }
                            if (oldTitle !== text.name) {
                                changes = changes + 'Title has changed from "' + oldTitle + '" to "' + text.name + '"; ';
                            }
                            if (oldAnnotationHash !== newAnnotationHash) {
                                changes = changes + 'Annotation has changed from "' + oldAnnotation + '" to "' + newAnnotation + '"; ';
                            }
                            UpdateLogItem.create().exec(function(errr, logItem) {
                                logItem.updateDetectedAt = book.updateDetectedAt;
                                logItem.description = changes;
                                logItem.bookTitle = book.title;
                                logItem.authorName = writer.name;
                                book.logs.add(logItem);
                                writer.logs.add(logItem);
                                logItem.save(function(error1) {
                                    if (error1) {
                                        console.log('[authorStorage] Error> was unable to create UpdateLogItem object: ' + error1);
                                    } else {
                                        //console.log('> created UpdateLogItem');
                                        UpdateLogItem.publishCreate(logItem);
                                    }
                                    saveBook();
                                });
                            });
                        } else {
                            saveBook();
                        }


                    });


            };

            updateBooks(function(error) {
                if (error) {
                    return callback(error);
                }
                save(writer, function(er, result) {
                    if (er) {
                        return callback(er);
                    }
                    result.authorHasUpdates = writer.hasUpdates;
                    if (writer.hasUpdates && oldHasUpdatesStatus !== writer.hasUpdates) {
                        result.authorHasNewUpdates = true;
                    } else {
                        result.authorHasNewUpdates = false;
                    }
                    load(writer.path, site, function(err, dbWriter) {
                        //console.log('>>>>>>>>>>>>>>>' + dbWriter.id);
                        //Writer.publishUpdate(dbWriter.id, dbWriter);
                        result.writer = dbWriter;
                        callback(err, result);
                    });

                });
            });

        });
}

// load author with books
function load(authorPath, site, callback) {
    Writer.findOne({
        path: authorPath,
        siteTemplate: site.siteTemplate
    })
        .populate('books')
    // .then(function(writer) {
    // 	var logs = UpdateLogItem.find({
    // 		writer: writer.id
    // 	}).limit(10).then(function(logs) {
    // 		return logs;
    // 	});
    // 	return [writer, logs];
    // }).spread(function(writer,logs) {
    // 	writer.logs = logs;
    // 	callback(null, writer);
    // }).catch(function(err) {
    // 	return callback('[authorStorage] Error> ' + err);
    // });
    .exec(function(err, writer) {
        if (err) {
            return callback('[authorStorage] Error> ' + err);
        }
        if (!writer) {
            return callback('[authorStorage] Error> ' + 'was unable to find Writer object ' + authorPath);
        }
        callback(err, writer);
    });
}

function loadBy(params, callback) {
    Writer.find(params)
        .populate('books')
        .exec(function(err, writers) {
            if (err) {
                return callback('[authorStorage] Error> ' + err);
            }
            if (!writers) {
                return callback('[authorStorage] Error> ' + 'was unable to find Writers by params:' + JSON.stringify(params));

            }
            callback(err, writers);
        });
}

// update existing Writer object
function update(writer, callback) {
    // native Writer object from Waterline ORM model has its own save method
    writer.save(function(err) {
        if (err) {
            var e = '[authorStorage] Error updating (' + (writer.href) + ')> ' + err;
            console.log(e);
            return callback(e);
        } else {
            console.log('[authorStorage] SUCCESS saving updated ' + writer.path);
            // not to send to client in push fase
            writer.books = [];
            writer.logs = [];
            Writer.publishUpdate(writer.id, writer);
        }
        return callback();
    });

}

exports.save = save; // for parsed object after parsing. Not sutable for updating Writer object
exports.update = update; // update existing Writer object
exports.load = load;
exports.loadBy = loadBy;