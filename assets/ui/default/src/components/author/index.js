require('insert-css')(require('./style.css'))

module.exports = {

    template: require('./template.html'),
    replace: true,
    data: function() {
        return {
            name: this.name,
            hasUpdates: this.hasUpdates,
            Selected: ''
        }
    },
    computed: {
        show_status: function() {
            if (!this.checkUpdates)
                return true;
            return this.hasUpdates; // show status if author has updates or disabled (we show yellow circle)
        },
        update_status: function() {
            if (!this.checkUpdates)
                return 'warning';
            if (this.hasUpdates)
                return 'success';
            return '';
        },
        updateDetectedAt_moment: function() {
            return moment(this.updateDetectedAt).calendar();
        },
        updateDetectedAtFormated: function() {
            return moment(this.updateDetectedAt).format('DD MMMM YYYY, HH:mm');
        },
        checkUpdatesAtFormated: function() {
            return moment(this.checkUpdatesAt).format('DD-MM, HH:mm');
        },
        image: function() {
            return $utils.getAuthorImage(this);
        }

    },
    created: function() {
        var self = this;
        db.authors.on('update', function(authors) {
            var json_authors = authors.toJSON();
            _(json_authors).forEach(function(author) {
                if (author.id == self.id) {
                    self.recreateMenu(self); // after update author recreate menu (it may change items)
                    self.$dispatch('author_selected_forcerefresh', self); // repaint books list
                }
            });
        });
        db.authors.on('change', function(author) {
            if (author.id == self.id) {
                self.recreateMenu(self); // after update author recreate menu (it may change items)
                //self.$dispatch('author_selected_forcerefresh', self); // repaint books list
            }
        });
        this.$on('author_selected', this.author_selected);

        self.recreateMenu(self);

    },
    ready: function() {
        // this.recreateMenu(this);
    },

    methods: {

        recreateMenu: function(self) {

            var menuId = 'author-action-button-' + self.id;
            context.destroy('#' + menuId, menuId);

            var checkUpdates;
            if (self.checkUpdates) {
                checkUpdates = {
                    text: 'отключить проверку',
                    action: function(e) {
                        e.preventDefault();
                        window.socket.get("/writer/toggleCheckUpdates", {
                            id: self.id,
                            checkUpdates: 'false'
                        }, function gotResponse(resData, jwres) {
                            if (resData.error) {
                                $utils.showDialog('Ошибка переключения: ' + resData.error, 'Менеджер проверок');
                                self.checkUpdates = true; // set status back
                                self.recreateMenu(self);
                                return;
                            }
                            // self.recreateMenu(self);
                        });
                        self.checkUpdates = false; // to fast react
                        self.recreateMenu(self);
                    }
                };
            } else {
                checkUpdates = {
                    text: 'включить проверку',
                    action: function(e) {
                        e.preventDefault();
                        window.socket.get("/writer/toggleCheckUpdates", {
                            id: self.id,
                            checkUpdates: 'true'
                        }, function gotResponse(resData, jwres) {
                            if (resData.error) {
                                $utils.showDialog('Ошибка переключения: ' + resData.error, 'Менеджер проверок');
                                self.checkUpdates = false; // set status back
                                self.recreateMenu(self);
                                return;
                            }
                            // self.recreateMenu(self);
                        });
                        self.checkUpdates = true; // to fast react
                        self.recreateMenu(self);
                    }
                }
            }

            var menuObjects = [{
                    text: 'проверить сейчас',
                    action: function(e) {
                        e.preventDefault();
                        window.socket.get("/writer/checkAuthor", {
                            id: self.id
                        }, function gotResponse(resData, jwres) {
                            if (resData.error) {
                                $utils.showDialog('Ошибка запуска проверки: ' + resData.error, 'Менеджер проверок');
                            }
                        });
                    }
                },
                checkUpdates, {
                    text: 'удалить',
                    action: function(e) {
                        e.preventDefault();
                        console.log(e);
                        alert('удалить ' + self.name);
                    }
                }
            ];

            if (self.hasUpdates) {
                menuObjects.unshift({
                    text: 'прочитано',
                    action: function(e) {
                        e.preventDefault();
                        window.socket.get("/writer/toggleHasUpdates", {
                            id: self.id,
                            hasUpdates: 'false'
                        }, function gotResponse(resData, jwres) {
                            if (resData.error) {
                                $utils.showDialog('Ошибка переключения: ' + resData.error, 'Менеджер проверок', self);
                                self.hasUpdates = true; // set status back
                                self.recreateMenu(self);
                            } else {
                                //self.$dispatch('author_selected_forcerefresh', self);
                            }
                        });
                        self.hasUpdates = false;
                        self.recreateMenu(self);
                    }
                });
            }


            menuObjects.unshift({
                header: 'выберите действие над автором'
            });

            context.attach('#' + menuId, menuObjects, menuId);

        },

        selected: function() {
            this.Selected = 'selected';
            this.$dispatch('author_selected', this);
        },
        author_selected: function(author, force_refresh) {
            if (this.id != author.id) { // do not update books if updated author is not we are
                this.Selected = '';
            }
        }
        //this.$dispatch('leftsidemenu_changes', item); 
    }
}