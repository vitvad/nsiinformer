// require('insert-css')(require('./style.css'))

module.exports = {
    template: require('./template.html'),
    replace: true,
    created: function() {
        theme_init.scrollers();
        if ($(window).width() < 1025) {
            $(".sidebar-left").removeClass("sidebar-nicescroller");
            $(".sidebar-right").removeClass("right-sidebar-nicescroller");
            $(".nav-dropdown-content").removeClass("scroll-nav-dropdown");
        }
    },
    data: function() {
        return {}
    },
    methods: {
        onClick: function(event, item) {        
            this.$dispatch('leftsidemenu_changes', item);    
            $('.sidebar-left li').removeClass('active');
            $(event.target).closest('li').addClass('active');
            var checkElement = $(event.target).next();
            if ((checkElement.is('ul')) && (checkElement.is(':visible'))) {
                $(event.target).closest('li').removeClass('active');
                checkElement.slideUp('fast');
            }
            if ((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
                $('.sidebar-left ul.sidebar-menu ul:visible').slideUp('fast');
                checkElement.slideDown('fast');
            }
            if ($(event.target).closest('li').find('ul').children().length == 0) {
                return true;
            } else {
                return false;
            }
        }
    }
}