$utils = {

    ToggleLeftSideMenu: function(container) {
        $states.set('pageLayoutToggle', !$states.get('pageLayoutToggle'));
        $(".top-navbar").toggleClass("toggle");
        $(".sidebar-left").toggleClass("toggle");
        $(".page-content").toggleClass("toggle");
        $(".icon-dinamic").toggleClass("rotate-180");

        if ($(window).width() > 991) {
            if ($(".sidebar-right").hasClass("toggle-left") === true) {
                $(".sidebar-right").removeClass("toggle-left");
                $(".top-navbar").removeClass("toggle-left");
                $(".page-content").removeClass("toggle-left");
                $(".sidebar-left").removeClass("toggle-left");
                if ($(".sidebar-left").hasClass("toggle") === true) {
                    $(".sidebar-left").removeClass("toggle");
                }
                if ($(".page-content").hasClass("toggle") === true) {
                    $(".page-content").removeClass("toggle");
                }
            }
        }
        Signals.signal('layoutUpdated');
    },

    toggleAuthorsList: function(){
        $('.authors-list').toggleClass('collapsed');
        $('.authors-list-title').toggleClass('collapsed');
        $('.btn-authors-plus').toggleClass('collapsed');
        $('.books-list').toggleClass('expanded');
        $states.authorsListWidth = $('.authors-list').hasClass('collapsed') ?  57 : 300 ;
        Signals.signal('layoutUpdated');
    },

    showDialog: function(body, title) {       
        $states.get('dialog_instance').show(body,title);
    },
    
    showInputDialog: function(title, description, input) {       
        $states.get('input_dialog_instance').show(title, description, input);
    },

    getAuthorImage: function(author){
        if (author.imageUrl) {
                return 'http://' + author.site + author.path + '/' + author.imageUrl;
            }
            return 'src/img/avatar/avatar.jpg';
    },
    getAuthorImageByAuthorUrl: function(authorUrl){
        return 'src/img/avatar/avatar.jpg';
    }
};